# AWS Immersion Day #

일정 : 4월 12(월) ~ 13(화) </br>
장소 : 웨비나 (온라인)

강의 내용
---
#### 13일 (이론) ####
* Compute – EC2 Overview, EC2 이해 및 인스턴스 선택하기, 컴퓨팅 관련 추가 서비스 (오토스케일링 구성, 모니터링 등), 서버리스 컴퓨팅
* Network – default VPC와 VPC 구성 요소, VPC 디자인 원칙, Multi VPC, VPC 엔드포인트
* Storage – AWS 스토리지 기본 소개, AWS 블록 스토리지, AWS 파일 스토리지, AWS 오브젝트 스토리지
* Database – AWS 데이터베이스 서비스 소개, FAQ로 알아보는 AWS 데이터베이스 서비스, 모범 사례 및 실전 고객 사례
* Security – AWS 보안, IAM 이해 및 모범 사례 소개, AWS 로깅 및 관리를 위한 CloudTrail와 CloudWatch

#### 14일 (실습) ####
* Network – VPC 생성, 추가 서브넷 생성, 라우트 테이블 수정 테스트
* Compute – EC2 생성, 오토 스케일링, 부하 테스트
* Database – Aurora 생성, 웹앱 서버와 DB 연결, RDS Failover 테스트
* Storage – S3로 정적 웹 호스팅하기, S3 버킷 버저닝 테스트
* Security– IAM 기본 구성, IAM 정책 (Policy)과 권한 (Permission)
---
#### 실습 참고 사이트 ####
* [VPC, EC2, DB, Storage 실습](https://kr-id-general.workshop.aws/ko/network/create_vpc.html)
* [IAM 정책 실습](https://whchoi98.gitbook.io/aws-iam/iam-basic)
* [강의 전체 자료(pdf,동영상)](https://www.amazon.com/clouddrive/share/VGSD0efp7ZaYp6RWxLVj4s5IMyuLKlQOghUuNCMuYLs?_encoding=UTF8&%2AVersion%2A=1&%2Aentries%2A=0&mgh=1)
